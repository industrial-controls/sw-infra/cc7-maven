ARG FROM
FROM $FROM

ARG MAVEN_URL=https://cern.ch/maven/apache-maven-3.6.2-bin-cern-settings.zip

RUN yum update -y && yum install -y unzip git sshpass && \
    yum clean all

RUN curl -L -o /tmp/mvn.zip $MAVEN_URL && \
    mkdir /opt/maven && \
    mkdir /build && \
    chmod 777 /build && \
    unzip -d /opt/maven /tmp/mvn.zip  && \
    f=(/opt/maven/*) && mv /opt/maven/*/* /opt/maven && rmdir "${f[@]}" && \
    rm /tmp/mvn.zip

ENV MVN_HOME=/opt/maven
# Works for JDK and JRE installations
ENV PATH $PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin:$MVN_HOME/bin

RUN adduser maven

USER maven

WORKDIR /build
ENTRYPOINT ["/opt/maven/bin/mvn"]
